# -*- coding: utf-8 -*-
# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html
import scrapy
from scrapy.item import Item, Field

class HtmlPage(Item):
    url = Field()
    domain = Field()
    title = Field()
    content = Field()
    file_urls = Field()
    ctime = Field()
    utime = Field()
    in_links = Field()
    out_links = Field()
    tags = Field()
    parseStatus = Field()
    crawlStatus = Field()
