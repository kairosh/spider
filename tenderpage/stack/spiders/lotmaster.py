# -*- encoding: utf-8 -*-

from types import StringType
from types import ListType
from scrapy.contrib.spiders import CrawlSpider, Rule
from scrapy.contrib.linkextractors.sgml import SgmlLinkExtractor
from scrapy.contrib.loader.processor import TakeFirst
from scrapy.contrib.loader import XPathItemLoader
from scrapy.selector import HtmlXPathSelector

from scrapy.utils.response import open_in_browser
from scrapy.linkextractors import LinkExtractor
from scrapy.selector import Selector
import logging
from datetime import datetime

from stack.items import  HtmlPage
import scrapy

class OrphanSpider(CrawlSpider):
    name = "lotmasterkz"
    allowed_domains = ["kz.lotmaster.kz"]
    start_urls = ["http://kz.lotmaster.kz/index.php?where=all&lot_place=%25&bazalotov=%25&metod=%25&searchph=&searchstyle=tochno&summaot=&summado=&avans=%25&date=&lotspp=5000"]
    rules = (
        Rule(LinkExtractor(allow=('page=\d+',)), follow=True),
        Rule(LinkExtractor(allow=('lot=\d+',)), callback='parse_item'),
    )

    def parse2(self, response):
        yield scrapy.FormRequest.from_response(response,
            formname='log4in_form_head',
            formdata={'user_login': 'kairo', 'user_password': 'kombinat65'},
            callback=self.parse_item)

    def parse_item(self, response):
        #open_in_browser(response)
        logging.info('parse Tender Page------------- %s', response.url)
        item = HtmlPage()
        #tender_id = self.get_query_param(response.url,'tenderid')
        body = unicode(response.body.decode(response.encoding)).encode('utf-8')
        item['url'] = response.url
        item['title'] = response.xpath('/html/head/title/text()').extract()[0]
        item['file_urls'] = response.xpath('//*[@id="Content"]/table/tr/td[1]/p/a/@href').extract()
        # logging.info(response.xpath('//*[@id="Content"]/table/tr/td[1]/p/a/@href').extract());
        # logging.info('-------------------------')
        # logging.info(response.xpath('//*[@id="Content"]/table/tr/td[1]/p/a').extract());
        # logging.info('-------------------------')
        # logging.info(response.xpath('//*[@id="Content"]/table/tr/td[1]/p').extract());
        # logging.info('-------------------------')
        # logging.info(response.xpath('//*[@id="Content"]/table/tr/td[1]').extract());
        # logging.info('-------------------------')
        # logging.info(response.xpath('//*[@id="Content"]/table/tr/td').extract());
        # logging.info('-------------------------')
        # logging.info(response.xpath('//*[@id="Content"]/table/tr').extract());
        # logging.info('-------------------------')

        item['content'] = body
        item['ctime'] = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        item['utime'] = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        #item['in_links'] = None
        #item['out_links'] = None
        item['parseStatus'] = 'unparsed'
        item['crawlStatus'] = 'actual'
        #item['tags'] = ['tender',type] #,tender_id

        return  item
